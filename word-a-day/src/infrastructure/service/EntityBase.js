'use strict';

const R = require('ramda');
const uuidV4 = require('uuid/v4');

function EntityBase(attributes, values) {
  const data = Object.assign(R.pickBy(R.identity, attributes), R.pickBy(R.identity, values || {}));

  if (!data.id) {
    data.id = uuidV4();
    data._isNew = true;
  }

  const that = data;

  that.toJSON = () => R.pick(R.keys(attributes), that);

  return that;
}

module.exports = EntityBase;
