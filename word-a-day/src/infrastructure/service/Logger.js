'use strict';

const winston = require('winston');

function Logger(label) {
  const that = {};

  const log = new winston.Logger({
    transports: [
      new winston.transports.Console({
        label: label,
        colorize: true,
        timestamp: () => new Date().toISOString()
      })
    ]
  });

  function info() {
    const args = Array.prototype.slice.call(arguments);
    log.info.apply(log, args);
  }

  function warn() {
    const args = Array.prototype.slice.call(arguments);
    log.warn.apply(log, args);
  }

  function error() {
    const args = Array.prototype.slice.call(arguments);
    log.error.apply(log, args);
  }

  that.info = info;
  that.warn = warn;
  that.error = error;

  return that;
}

module.exports = Logger;
