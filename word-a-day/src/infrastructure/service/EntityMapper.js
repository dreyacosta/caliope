'use strict';

const R = require('ramda');

function EntityMapper(mapper) {
  const that = {};

  if (!mapper || !mapper.toDatabase || !mapper.toEntity) {
    throw Error('Invalid mapper');
  }

  that.toDatabase = (entity) => {
    const entityDatabaseDto = {};

    return R.keys(entity).reduce((entityDatabaseDto, entityKey) => {
      const databaseKey = mapper.toDatabase[entityKey];

      if (!databaseKey) return entityDatabaseDto;

      entityDatabaseDto[databaseKey] = entity[entityKey];

      return entityDatabaseDto;
    }, entityDatabaseDto);
  };

  that.toEntity = (databaseDto) => {
    const entityDto = {};

    return R.keys(databaseDto).reduce((entityDto, databaseDtoKey) => {
      const entityKey = mapper.toEntity[databaseDtoKey];

      if (!entityKey) return entityDto;

      entityDto[entityKey] = databaseDto[databaseDtoKey];

      return entityDto;
    }, entityDto);
  };

  return that;
}

module.exports = EntityMapper;
