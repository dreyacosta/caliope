'use strict';

const R = require('ramda');

const viewWords = require('../../../application/service/word').viewWords;
const viewWordsForEdit = require('../../../application/service/word').viewWordsForEdit;
const viewWord = require('../../../application/service/word').viewWord;
const createWord = require('../../../application/service/word').createWord;
const updateWord = require('../../../application/service/word').updateWord;

module.exports.landing = (req, res) => {
  viewWords((error, results) => {
    res.json(results);
  });
};

module.exports.getWord = (req, res) => {
  viewWord(req.params.word, (error, result) => {
    res.json(result);
  });
};

module.exports.createWord = (req, res) => {
  const attributes = R.pick(['word', 'definition', 'publishedAt', 'synonyms'], req.body);
  if (attributes.synonyms) attributes.synonyms = R.split(',', attributes.synonyms);

  createWord(attributes, (error, result) => {
    if (error instanceof Error) {
      return res.status(500).json(error.message);
    }

    res.status(201).json({ success: true });
  });
};

module.exports.editWords = (req, res) => {
  viewWordsForEdit((error, results) => {
    res.json(results);
  });
};

module.exports.updateWord = (req, res) => {
  const wordId = R.prop(['id'], req.body);
  const attributes = R.pick(['word', 'definition', 'publishedAt'], req.body);

  updateWord(wordId, attributes, (error, result) => {
    if (error instanceof Error) {
      return res.status(500).json(error.message);
    }

    res.json({ success: true });
  });
};
