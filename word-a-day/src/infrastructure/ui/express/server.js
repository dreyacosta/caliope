'use strict';

const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const words = require('./controllers');
const isAuthorized = require('./middlewares').isAuthorized;
const app = express();

module.exports = (callback) => {
  if (!callback) {
    callback = () => {};
  }

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(morgan('combined'));

  app.get('/words', words.landing);
  app.get('/word/:word', words.getWord);
  app.post('/word', isAuthorized, words.createWord);
  app.get('/edit/words', words.editWords);
  app.put('/update/word', isAuthorized, words.updateWord);

  app.use((req, res) => res.status(404).json());

  const server = app.listen(3000, (error) => {
    if (error) throw new Error(error);
    console.log('Server started on port 3000');
    callback(null, server, app);
  });
};
