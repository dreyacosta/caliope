'use strict';

const CONFIG = require('config');

module.exports.isAuthorized = (req, res, next) => {
  const body = req.body || {};
  const token = body.validationToken || null;

  if (!token || token !== CONFIG.validationToken) {
    return res.status(401).json({ message: 'Unauthorized' });
  }

  next();
};
