'use strict';

function FileRepository(Store, mappers) {
  const connections = {};

  return (databaseName, collectionName) => {
    const db = getDatabaseConnection(databaseName);
    const mapper = mappers[collectionName];

    if (!mapper) throw Error('Database mapper not found');

    const that = {};

    that.save = (entity, cb) => {
      const databaseDto = mapper.toDatabase(entity);

      db.insert(databaseDto, (error, result) => {
        if (error && error.errorType === 'uniqueViolated') {
          return cb(null, mapper.toEntity(result));
        }

        if (error) {
          return cb(error);
        }

        cb(null, mapper.toEntity(result));
      });
    };

    that.update = (entity, cb) => {
      const databaseDto = mapper.toDatabase(entity);
      const query = { _id: databaseDto._id };
      const update = { $set: databaseDto };
      const options = {};

      db.update(query, update, options, (error) => {
        if (error) return cb(error);

        cb(null, mapper.toEntity(databaseDto));
      });
    };

    that.all = (cb) => {
      db.find({}, (error, results) => {
        if (error) return cb(error);

        const entities = results.map((result) => mapper.toEntity(result));

        cb(null, entities);
      });
    };

    that.allSorted = (sort, cb) => {
      const query = {};
      const sortDto = mapper.toDatabase(sort);

      db.find(query).sort(sortDto).exec((error, results) => {
        if (error) return cb(error);

        const entities = results.map((result) => mapper.toEntity(result));

        cb(null, entities);
      });
    };

    that.findOne = (query, cb) => {
      const queryDto = mapper.toDatabase(query);

      db.findOne(queryDto, (error, result) => {
        if (error) return cb(error);

        if (!result) return cb(null, null);

        cb(null, mapper.toEntity(result));
      });
    };

    that.removeAll = (cb) => db.remove({}, { multi: true }, cb);

    return that;
  };

  function getDatabaseConnection(databaseName) {
    const connectionOptions = {
      filename: `./data/${databaseName}.json`,
      autoload: true
    };

    connections[databaseName] = connections[databaseName] || new Store(connectionOptions);

    return connections[databaseName];
  }
}

module.exports = FileRepository;
