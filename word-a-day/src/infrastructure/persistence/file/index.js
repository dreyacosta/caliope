'use strict';

const Nedb = require('nedb');
const mappers = require('./mappings');
const FileRepository = require('./FileRepository');

module.exports = FileRepository(Nedb, mappers);
