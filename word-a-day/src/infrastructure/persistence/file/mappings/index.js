'use strict';

const EntityMapper = require('../../../service/EntityMapper');

const word = require('./word');

module.exports = {
  word: EntityMapper(word)
};
