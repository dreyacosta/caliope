'use strict';

module.exports = {
  toDatabase: {
    id: '_id',
    word: 'word_name',
    definition: 'definition',
    synonyms: 'synonyms',
    publishedAt: 'publishedAt',
    createdAt: 'createdAt'
  },
  toEntity: {
    _id: 'id',
    word_name: 'word',
    definition: 'definition',
    synonyms: 'synonyms',
    publishedAt: 'publishedAt',
    createdAt: 'createdAt'
  }
};
