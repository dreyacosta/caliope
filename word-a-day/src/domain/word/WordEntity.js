'use strict';

const EntityBase = require('../../infrastructure/service/EntityBase');

function WordEntity(values) {
  const attributes = {
    id: null,
    word: null,
    wordLowerCase: values.word ? values.word.toLowerCase() : null,
    definition: null,
    publishedAt: new Date().toISOString(),
    synonyms: [],
    inContext: [],
    createdAt: new Date().toISOString()
  };

  const that = EntityBase(attributes, values);

  that.setCreateAt = () => {
    that.createdAt = new Date().toISOString();
  };

  that.setWordName = (name) => {
    that.word = name || that.word;
  };

  that.setDefinition = (definition) => {
    that.definition = definition || that.definition;
  };

  that.setSynonyms = (synonyms) => {
    that.synonyms = synonyms || that.synonyms;
  };

  that.setPublishedAt = (date) => {
    that.publishedAt = date || that.publishedAt || new Date();
    if (that.publishedAt) {
      that.publishedAt = new Date(that.publishedAt).toISOString();
    }
  };

  that.updateAttributes = (update) => {
    Object.keys(attributes).forEach((key) => {
      const updateValue = update[key];

      if (updateValue) {
        that[key] = updateValue;
      }
    });
    that.setPublishedAt(that.publishedAt);
  };

  that.getId = () => that.id;

  return that;
}

module.exports = WordEntity;
