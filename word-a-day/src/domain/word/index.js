'use strict';

const wordDatabaseName = require('config').db.WordRepository.name;
const wordCollectionName = require('config').db.WordRepository.collection;
const FileRepository = require('../../infrastructure/persistence/file');
const fileWordRepository = FileRepository(wordDatabaseName, wordCollectionName);
const WordRepository = require('./WordRepository');

module.exports = {
  wordRepository: WordRepository(fileWordRepository)
};
