'use strict';

const Word = require('./WordEntity');

function WordRepository(fileRepository) {
  const that = {};

  that.save = (word, cb) => {
    fileRepository.save(word, (error, wordDto) => {
      if (error) {
        return cb(error);
      }

      cb(null, Word(wordDto));
    });
  }

  that.update = (word, cb) => {
    fileRepository.update(word, (error, wordDto) => {
      if (error) {
        return cb(error);
      }

      cb(null, Word(wordDto));
    });
  }

  that.getWordsSortByPublishedDate = (cb) => {
    fileRepository.allSorted({ publishedAt: -1 }, (error, wordsDto) => {
      if (error) {
        return cb(error);
      }

      cb(null, wordsDto.map((wordDto) => Word(wordDto)));
    });
  };

  that.findById = (id, cb) => {
    const query = { id: id };

    return fileRepository.findOne(query, (error, wordDto) => {
      if (error) {
        return cb(error);
      }

      if (!wordDto) {
        return cb(new Error('Word not found'));
      }

      cb(null, Word(wordDto));
    });
  };

  that.wordNameExists = (wordName, cb) => {
    const query = { word: wordName };

    fileRepository.findOne(query, (error, wordDto) => {
      if (error) {
        return cb(error);
      }

      if (wordDto) {
        return cb(new Error('WordAlreadyExist'));
      }

      cb();
    });
  };

  that.findByWordName = (wordName, cb) => {
    const query = { word: wordName };

    fileRepository.findOne(query, (error, wordDto) => {
      if (error) {
        return cb(error);
      }

      if (!wordDto) {
        return cb(new Error('Word not found'));
      }

      cb(null, Word(wordDto));
    });
  };

  that.removeAll = (cb) => fileRepository.removeAll(cb);

  return that;
}

module.exports = WordRepository;
