'use strict';

const log = require('../../../infrastructure/service/Logger')('ViewWordsForEdit');

function ViewWordsForEdit(wordRepository) {
  return (cb) => {
    wordRepository.getWordsSortByPublishedDate((error, words) => {
      log.info(words.map(word => word.toJSON()));
      cb(null, { words: words });
    });
  };
}

module.exports = ViewWordsForEdit;
