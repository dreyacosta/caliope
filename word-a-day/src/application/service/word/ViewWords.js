'use strict';

const log = require('../../../infrastructure/service/Logger')('ViewWords');

function ViewWords(wordRepository) {
  return (cb) => {
    wordRepository.getWordsSortByPublishedDate((error, words) => {
      if (error) return cb(error);

      log.info('Words:', words.map(word => word.toJSON()));

      cb(null, { word: words[0] || null, words: words.slice(1, 3) });
    });
  };
}

module.exports = ViewWords;
