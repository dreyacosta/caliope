'use strict';

const log = require('../../../infrastructure/service/Logger')('UpdateWord');

function UpdateWord(wordRepository) {
  return (id, attributes, cb) => {
    wordRepository.findById(id, (error, word) => {
      if (error) return cb(error);

      attributes = attributes || {};
      const definition = attributes.definition || null;
      const synonyms = attributes.synonyms || null;

      if (definition) word.setDefinition(definition);
      if (synonyms) word.setSynonyms(synonyms);

      wordRepository.update(word, (error, word) => {
        if (error) return cb(error);

        cb(null, { word: word });
      });
    });
  };
}

module.exports = UpdateWord;
