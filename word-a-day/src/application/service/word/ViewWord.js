'use strict';

const log = require('../../../infrastructure/service/Logger')('ViewWord');

function ViewWord(wordRepository) {
  return (wordName, cb) => {
    const word = wordRepository.findByWordName(wordName, (error, word) => {
      if (error) return cb(error);

      log.info(word.toJSON());

      cb(null, { word: word });
    });
  };
}

module.exports = ViewWord;
