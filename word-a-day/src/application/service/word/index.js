'use strict';

const wordRepository = require('../../../../src/domain/word').wordRepository;
const ViewWords = require('./ViewWords');
const viewWords = ViewWords(wordRepository);

const ViewWordsForEdit = require('./ViewWordsForEdit');
const viewWordsForEdit = ViewWordsForEdit(wordRepository);

const ViewWord = require('./ViewWord');
const viewWord = ViewWord(wordRepository);

const Word = require('../../../../src/domain/word/WordEntity');
const CreateWord = require('./CreateWord');
const createWord = CreateWord(wordRepository, Word);

const UpdateWord = require('./UpdateWord');
const updateWord = UpdateWord(wordRepository);

module.exports = {
  viewWords: viewWords,
  viewWordsForEdit: viewWordsForEdit,
  viewWord: viewWord,
  createWord: createWord,
  updateWord: updateWord
};
