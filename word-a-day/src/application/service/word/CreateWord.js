'use strict';

const log = require('../../../infrastructure/service/Logger')('CreateWord');

function CreateWord(wordRepository, Word) {
  return (attributes, cb) => {
    if (!attributes.word) {
      log.error('WordNameNotFound', attributes);
      return cb(new Error('WordNameNotFound'));
    }

    wordRepository.wordNameExists(attributes.word, (error) => {
      if (error) return cb(error);

      const newWord = Word(attributes);
      wordRepository.save(newWord, (error, word) => {
        if (error) return cb(error);

        cb(null, { word: word });
      });
    });
  };
}

module.exports = CreateWord;
