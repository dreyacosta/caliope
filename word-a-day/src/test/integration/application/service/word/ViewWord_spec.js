'use strict';

const should = require('should');

const Word = require(`${paths.domain}/word/WordEntity`);
const wordRepository = require(`${paths.domain}/word`).wordRepository;

const viewWord = require(`${paths.applicationService}/word`).viewWord;

describe('View word', () => {
  beforeEach(wordRepository.removeAll);

  it('should return an error if word not found', (done) => {
    const wordName = 'Disidencia';

    viewWord(wordName, (error) => {
      should(error).instanceof(Error);
      should(error.message).be.eql('Word not found');

      done();
    });
  });

  it('should return a word', (done) => {
    const word = Word({
      word: 'Disidencia'
    });

    wordRepository.save(word, (error, doc) => {
      const wordName = 'Disidencia';

      viewWord(wordName, (error, response) => {
        should(response.word.word).be.eql('Disidencia');

        done(error);
      });
    });
  });
});
