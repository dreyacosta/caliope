'use strict';

const should = require('should');

const Word = require(`${paths.domain}/word/WordEntity`);
const wordRepository = require(`${paths.domain}/word`).wordRepository;

const createWord = require(`${paths.applicationService}/word`).createWord;

describe('View words', () => {
  beforeEach(wordRepository.removeAll);

  it('should return an error if word name not provided', (done) => {
    const attributes = {};

    createWord(attributes, (error, response) => {
      should(error).instanceof(Error);
      should(error.message).be.eql('WordNameNotFound');

      done();
    });
  });

  it('should return an error if word name already exists', (done) => {
    const word = Word({
      word: 'Disidencia',
      definition: 'Desacuerdo de opiniones.'
    });

    wordRepository.save(word, (error, word) => {
      const attributes = {
        word: 'Disidencia'
      };

      createWord(attributes, (error, response) => {
        should(error).instanceof(Error);
        should(error.message).be.eql('WordAlreadyExist');

        done();
      });
    });
  });

  it('should save a new word', (done) => {
    const attributes = {
      word: 'Disidencia'
    };

    createWord(attributes, (error, response) => {
      should(response.word.word).be.eql('Disidencia');
      should.exists(response.word.id);

      done(error);
    });
  });
});
