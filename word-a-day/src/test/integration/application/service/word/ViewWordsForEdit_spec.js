'use strict';

const should = require('should');

const Word = require(`${paths.domain}/word/WordEntity`);
const wordRepository = require(`${paths.domain}/word`).wordRepository;

const viewWordsForEdit = require(`${paths.applicationService}/word`).viewWordsForEdit;

describe('View words', () => {
  beforeEach(wordRepository.removeAll);

  it('should return empty words', (done) => {
    viewWordsForEdit((error, response) => {
      should(response.words).be.eql([]);

      done(error);
    });
  });

  it('should return words', (done) => {
    const word = Word({
      word: 'Disidencia',
      definition: 'Desacuerdo de opiniones.'
    });

    wordRepository.save(word, (error, word) => {
      viewWordsForEdit((error, response) => {
        should(response.words.length).be.eql(1);
        should(response.words[0].word).be.eql('Disidencia');

        done(error);
      });
    });
  });
});
