'use strict';

const should = require('should');

const Word = require(`${paths.domain}/word/WordEntity`);
const wordRepository = require(`${paths.domain}/word`).wordRepository;

const updateWord = require(`${paths.applicationService}/word`).updateWord;

describe('Update word', () => {
  beforeEach(wordRepository.removeAll);

  it('should return an error if word not found', (done) => {
    const wordId = 'e1d964e4-7d63-4d6a-8ed5-472d7151f2c1';
    const update = null;

    updateWord(wordId, update, (error) => {
      should(error).instanceof(Error);
      should(error.message).be.eql('Word not found');

      done();
    });
  });

  context('update', () => {
    const word = Word({
      word: 'Disidencia',
      definition: 'Desacuerdo de opiniones.',
      synonyms: 'Discrepancia'
    });

    beforeEach((done) => wordRepository.save(word, done));

    it('definition', (done) => {
      const wordId = word.id;
      const update = { definition: 'Cisma, desacuerdo, desavenencia.' };

      updateWord(wordId, update, (error, response) => {
        should(response.word.word).be.eql('Disidencia');
        should(response.word.definition).be.eql('Cisma, desacuerdo, desavenencia.');
        should(response.word.synonyms).be.eql('Discrepancia');

        done(error);
      });
    });

    it('synonyms', (done) => {
      const wordId = word.id;
      const update = { synonyms: 'Discrepancia, opisición' };

      updateWord(wordId, update, (error, response) => {
        should(response.word.word).be.eql('Disidencia');
        should(response.word.definition).be.eql('Desacuerdo de opiniones.');
        should(response.word.synonyms).be.eql('Discrepancia, opisición');

        done(error);
      });
    });
  });
});
