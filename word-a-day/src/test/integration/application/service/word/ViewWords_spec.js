'use strict';

const should = require('should');

const Word = require(`${paths.domain}/word/WordEntity`);
const wordRepository = require(`${paths.domain}/word`).wordRepository;

const viewWords = require(`${paths.applicationService}/word`).viewWords;

describe('View words', () => {
  beforeEach(wordRepository.removeAll);

  it('should return an empty words', (done) => {
    viewWords((error, response) => {
      should.not.exists(response.word);
      should(response.words).be.eql([]);

      done(error);
    });
  });

  it('should return words', (done) => {
    const word = Word({
      word: 'Disidencia',
      definition: 'Desacuerdo de opiniones.'
    });

    wordRepository.save(word, (error, word) => {
      viewWords((error, response) => {
        should(response.word.word).be.eql('Disidencia');
        should(response.words).be.eql([]);

        done(error);
      });
    });
  });
});
