'use strict';

const should = require('should');

const Word = require(`${paths.domain}/word/WordEntity`);
const wordRepository = require(`${paths.domain}/word`).wordRepository;

describe('Word repository', () => {
  beforeEach(wordRepository.removeAll);

  it('should save entity', (done) => {
    const word = Word({
      word: 'Disidencia',
      definition: 'Desacuerdo de opiniones.'
    });

    wordRepository.save(word, (error, wordSaved) => {
      should(wordSaved.getId).be.a.Function();

      done();
    });
  });

  it('should update entity', (done) => {
    const word = Word({
      word: 'Disidencia',
      definition: 'Desacuerdo de opiniones.'
    });

    wordRepository.save(word, (error, wordSaved) => {
      wordSaved.setDefinition('Cisma, desacuerdo, desavenencia.');

      wordRepository.update(wordSaved, (error, wordUpdated) => {
        should(wordUpdated.getId).be.a.Function();

        done();
      });
    });
  });

  it('should getWordsSortByPublishedDate entities', (done) => {
    const wordDisidencia = Word({
      word: 'Disidencia',
      definition: 'Desacuerdo de opiniones.',
      publishedAt: new Date('2017-06-21').toISOString()
    });

    wordRepository.save(wordDisidencia, (error, wordSaved) => {
      const wordLaudable = Word({
        word: 'Laudable',
        definition: 'Que merece ser loado o alabado.',
        publishedAt: new Date('2017-06-23').toISOString()
      });

      wordRepository.save(wordLaudable, (error, wordSaved) => {
        wordRepository.getWordsSortByPublishedDate((error, words) => {
          should(words[0].word).be.eql('Laudable');
          should(words[1].word).be.eql('Disidencia');

          done();
        });
      });
    });
  });

  it('should find entity by id', (done) => {
    const wordDisidencia = Word({
      word: 'Disidencia',
      definition: 'Desacuerdo de opiniones.'
    });

    wordRepository.save(wordDisidencia, (error, wordSaved) => {
      wordRepository.findById(wordSaved.getId(), (error, word) => {
        should(word.getId).be.a.Function();

        done();
      });
    });
  });

  it('should find entity by word name', (done) => {
    const wordDisidencia = Word({
      word: 'Disidencia',
      definition: 'Desacuerdo de opiniones.'
    });

    wordRepository.save(wordDisidencia, (error, wordSaved) => {
      wordRepository.findByWordName('Disidencia', (error, word) => {
        should(word.getId).be.a.Function();

        done();
      });
    });
  });
});
