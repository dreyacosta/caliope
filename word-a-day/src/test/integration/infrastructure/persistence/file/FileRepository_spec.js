'use strict';

const should = require('should');

const FileRepository = require(`${paths.infrastructurePersistence}/file`);
const mappers = require(`${paths.infrastructurePersistence}/file/mappings`);

describe('File repository', () => {
  const databaseName = 'test-database';
  const collectionName = 'word';
  const fileRepository = FileRepository(databaseName, collectionName);

  beforeEach(fileRepository.removeAll);

  it('should save entity', (done) => {
    const entity = EntityStub({
      id: '1',
      word: 'Disidencia',
      definition: 'Desacuerdo de opiniones.'
    });

    fileRepository.save(entity, (error, entityDatabaseDto) => {
      should(entityDatabaseDto).be.eql({
        id: '1',
        word: 'Disidencia',
        definition: 'Desacuerdo de opiniones.'
      });

      done();
    });
  });

  it('should update entity', (done) => {
    const entity = EntityStub({
      id: '2',
      word: 'Disidencia',
      definition: 'Desacuerdo de opiniones.'
    });

    fileRepository.save(entity, (error, entityDatabaseDto) => {
      const entityUpdated = EntityStub(entityDatabaseDto);
      entityUpdated.setDefinition('Acción y efecto de disidir.');

      fileRepository.update(entityUpdated, (error, entityDatabaseUpdatedDto) => {
        should(entityDatabaseUpdatedDto).be.eql({
          id: '2',
          word: 'Disidencia',
          definition: 'Acción y efecto de disidir.'
        });

        done();
      });
    });
  });

  it('should find one entity', (done) => {
    const entity = EntityStub({
      id: '3',
      word: 'Disidencia',
      definition: 'Desacuerdo de opiniones.'
    });

    fileRepository.save(entity, (error, entityDatabaseDto) => {
      const entitySaved = EntityStub(entityDatabaseDto);
      const query = { word: entitySaved.getWordName() };

      fileRepository.findOne(query, (error, entityDatabaseFoundDto) => {
        should(entityDatabaseFoundDto).be.eql({
          id: '3',
          word: 'Disidencia',
          definition: 'Desacuerdo de opiniones.'
        });

        done();
      });
    });
  });

  it('should find all entities', (done) => {
    const entity = EntityStub({
      id: '4',
      word: 'Disidencia',
      definition: 'Desacuerdo de opiniones.'
    });

    fileRepository.save(entity, () => {
      fileRepository.all((error, entitiesDto) => {
        should(entitiesDto).be.eql([{
          id: '4',
          word: 'Disidencia',
          definition: 'Desacuerdo de opiniones.'
        }]);

        done();
      });
    });
  });

  function EntityStub(values) {
    const that = values;

    that.setDefinition = (definition) => {
      that.definition = definition;
      return that;
    };

    that.getWordName = () => {
      return that.word;
    };

    return values;
  }
});
