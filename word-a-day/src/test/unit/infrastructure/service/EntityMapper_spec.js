'use strict';

const should = require('should');

const EntityMapper = require(`${paths.infrastructureService}/EntityMapper`);

describe('Entity mapper', () => {
  it('should throw if no mapper', () => {
    should(EntityMapper).throw('Invalid mapper');
  });

  it('should throw if mapper does not have toDatabase', () => {
    // Arrange
    const mapper = {
      toEntity: {}
    };

    // Act
    function EntityMapperWrapper() {
      EntityMapper(mapper);
    }

    // Assert
    should(EntityMapperWrapper).throw('Invalid mapper');
  });

  it('should throw if mapper does not have toEntity', () => {
    // Arrange
    const mapper = {
      toDatabase: {}
    };

    // Act
    function EntityMapperWrapper() {
      EntityMapper(mapper);
    }

    // Assert
    should(EntityMapperWrapper).throw('Invalid mapper');
  });

  it('should not throw if mapper is complete', () => {
    // Arrange
    const mapper = {
      toDatabase: {},
      toEntity: {}
    };

    // Act
    function EntityMapperWrapper() {
      EntityMapper(mapper);
    }

    // Assert
    should(EntityMapperWrapper).not.throw();
  });

  it('should map entity to databaseDto', () => {
    // Arrange
    const mapper = {
      toDatabase: {
        id: '_id',
        name: 'name',
        username: 'user_name'
      },
      toEntity: {}
    };
    const entityMapper = EntityMapper(mapper);
    const entity = EntityStub({
      name: 'David',
      username: 'dreyacosta'
    });

    // Act
    const entityDatabaseDto = entityMapper.toDatabase(entity);

    // Assert
    should(entityDatabaseDto).be.eql({
      _id: '1',
      name: 'David',
      user_name: 'dreyacosta'
    });
  });

  it('should map from database to entityDto', () => {
    // Arrange
    const mapper = {
      toDatabase: {},
      toEntity: {
        _id: 'id',
        name: 'name',
        user_name: 'username'
      }
    };
    const entityMapper = EntityMapper(mapper);
    const databaseDto = {
      _id: '1',
      name: 'David',
      user_name: 'dreyacosta'
    };

    // Act
    const entityDto = entityMapper.toEntity(databaseDto);

    // Assert
    should(entityDto).be.eql({
      id: '1',
      name: 'David',
      username: 'dreyacosta'
    });
  });

  function EntityStub(values) {
    values.id = '1';
    return values;
  }
});
