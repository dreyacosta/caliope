'use strict';

const paths = {
  applicationService: `${__dirname}/../application/service`,
  domain: `${__dirname}/../domain`,
  infrastructurePersistence: `${__dirname}/../infrastructure/persistence`,
  infrastructureService: `${__dirname}/../infrastructure/service`
};

global.paths = paths;
