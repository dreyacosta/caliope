#!/bin/sh

new_deploy() {
  local STAGE=$1
  local FILES=$(_files $STAGE)

  FILES="web/test.js"
  echo "Files updated: $FILES"

  if [ ! "$FILES" ]; then
    echo "No files updated. Exit"
    exit 0
  fi

  local SERVICES=$(_services $FILES)

  SERVICES="web word-a-day word-random"
  if [ ! "$SERVICES" ]; then
    echo "No services to deploy. Exit"
    exit 0
  fi

  echo "Services to deploy: $SERVICES"

  for SERVICE in $SERVICES; do
    echo "Deploying... $SERVICE"
    deploy_service $SERVICE $STAGE
  done
}

deploy_service() {
  local SERVICE=$1
  local STAGE=$2
  local NETWORK=$SERVICES_NETWORK

  local VOLUME=""
  if [ $SERVICE == "word-a-day" ]; then
    VOLUME="--mount type=volume,volume-driver=cloudstor:aws,source=$WORD_A_DAY_VOLUME,destination=/srv/app/caliope/data"
    echo "word-a-day volume: $VOLUME"
  fi

  local TRAEFIK_LABEL=""
  local ENVS=""
  if [ $SERVICE == "web" ]; then
    TRAEFIK_LABEL=$WEB_LABELS
    echo "web labels: $TRAEFIK_LABEL"
    ENVS="--env WORD_A_DAY_URL=$WORD_A_DAY_URL --env WORD_RANDOM_URL=$WORD_RANDOM_URL"
  fi

  local DOCKER_BUILD_NAME="$SERVICE:$CI_COMMIT_SHA"
  local REPOSITORY="registry.gitlab.com/dreyacosta/caliope/$DOCKER_BUILD_NAME"
  docker build -t $REPOSITORY ./$SERVICE
  docker push $REPOSITORY

  local SWARM_SERVICE_NAME="$STAGE-$SERVICE"
  local SERVICE_NOT_EXISTS=$(docker service ls --filter name=$SWARM_SERVICE_NAME --quiet | wc -l)

  if [[ "$SERVICE_NOT_EXISTS" -eq 0 ]]; then
    echo "Creating service $SWARM_SERVICE_NAME"
    echo "COMMNAD: docker service create --name $SWARM_SERVICE_NAME $NETWORK $TRAEFIK_LABEL $ENVS $VOLUME $REPOSITORY"
    docker service create --name $SWARM_SERVICE_NAME $NETWORK $TRAEFIK_LABEL $ENVS $VOLUME $REPOSITORY
  else
    echo "Updating service $SWARM_SERVICE_NAME"
    echo "COMMAND: docker service update $SWARM_SERVICE_NAME --image $REPOSITORY"
    docker service update $SWARM_SERVICE_NAME --image $REPOSITORY
  fi

  docker service scale $SWARM_SERVICE_NAME=$REPLICAS
}

remove_service() {
  local SERVICE=$1
  local STAGE=$2
  echo "Removing service $SERVICE-$STAGE"
  docker service rm $SERVICE-$STAGE
}

_files() {
  local STAGE=$1
  local FILES=""

  if [ "$STAGE" == 'latest' ]; then
    FILES=$(git diff --name-only HEAD^ HEAD)
  else
    BRANCH_NAME=$(git symbolic-ref --short HEAD)
    INTEGRATION=master
    DIFF_POINT=$(git merge-base --fork-point $INTEGRATION $BRANCH_NAME)
    GIT_DIFF=$(git diff --name-only $DIFF_POINT..$BRANCH_NAME)
    FILES=$GIT_DIFF
  fi

  echo $FILES
}

_services() {
  local FILES=$@
  local SERVICES=""

  for DIRECTORY in $FILES; do
    DIRECTORY=$(dirname $DIRECTORY|cut -d "/" -f1)

    if [ "$DIRECTORY" == '.' ]; then
      TEMP="."
    elif [ "$DIRECTORY" == 'ci' ]; then
      TEMP="ci"
    else
      SERVICES="$SERVICES $DIRECTORY "
    fi
  done

  SERVICES=$(echo "$SERVICES"|tr ' ' '\n'|sort -u|tr '\n' ' ')
  echo $SERVICES
}
