'use strict';

const sass = require('node-sass');

module.exports = (app) => (req, res, next) => {
  sass.render({
    file: __dirname + '/default.scss',
    outputStyle: 'compressed'
  }, (error, result) => {
    if (error) return next(error);
    app.locals.style = result.css.toString().trim();
    next();
  });
};
