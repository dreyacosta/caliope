'use strict';

const hbs = require('express-hbs');
const instance = hbs.create();

module.exports = () => {
  const hbsOptions = {
    partialsDir: [__dirname],
    onCompile: (exhbs, source) => {
      return exhbs.handlebars.compile(source, { preventIndent: true });
    }
  };

  return instance.express4(hbsOptions);
};
