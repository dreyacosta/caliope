'use strict';

const request = require('request');

module.exports.landing = (req, res) => {
  request(`${process.env.WORD_A_DAY_URL}/words`, (error, response, body) => {
    res.render('landing', JSON.parse(body));
  });
};

module.exports.getWord = (req, res) => {
  request(`${process.env.WORD_A_DAY_URL}/word/` + req.params.word, (error, response, body) => {
    res.render('landing', JSON.parse(body));
  });
};

module.exports.addWordPage = (req, res) => res.render('word');

module.exports.createWord = (req, res) => {
  request.post(`${process.env.WORD_A_DAY_URL}/word`, { form: req.body }, (error, response, body) => {
    if (response.statusCode !== 201) {
      return res.send(error.message);
    }

    res.redirect('/add/word');
  });
};

module.exports.editWords = (req, res) => {
  request(`${process.env.WORD_A_DAY_URL}/edit/words`, (error, response, body) => {
    res.render('editWords', JSON.parse(body));
  });
};

module.exports.updateWord = (req, res) => {
  request.put(`${process.env.WORD_A_DAY_URL}/update/word`, { form: req.body }, (error, response, body) => {
    if (error) {
      return res.send(error.message);
    }

    res.redirect('/edit/words');
  });
};

module.exports.randomWord = (req, res) => {
  request(`${process.env.WORD_RANDOM_URL}`, function (error, response, body) {
    res.send(body);
  });
};
