'use strict';

const CONFIG = {
  validationToken: '1234'
};

module.exports.isAuthorized = (req, res, next) => {
  const body = req.body || {};
  const token = body.validationToken || null;

  if (!token || token !== CONFIG.validationToken) {
    return res.send('Unauthorized');
  }

  next();
};
