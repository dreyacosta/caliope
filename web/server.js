'use strict';

const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const ViewsEngine = require('./views');
const Styles = require('./stylesheets');
const words = require('./controllers');
const isAuthorized = require('./middlewares').isAuthorized;
const app = express();

module.exports = (callback) => {
  if (!callback) {
    callback = () => {};
  }

  app.engine('hbs', ViewsEngine());
  app.set('view engine', 'hbs');
  app.set('views', __dirname + '/views');
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(morgan('combined'));
  app.use(Styles(app));

  app.get('/', words.landing);
  app.get('/word/:word', words.getWord);
  app.get('/add/word', words.addWordPage);
  app.post('/word', words.createWord);
  app.get('/edit/words', words.editWords);
  app.post('/update/word', words.updateWord);
  app.get('/random', words.randomWord);

  const server = app.listen(5000, () => {
    console.log('Server started on port 5000');
    callback(null, server, app);
  });
};
