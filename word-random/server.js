'use strict';

const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const RandomWord = require('./RandomWord')();
const app = express();

module.exports = (callback) => {
  if (!callback) {
    callback = () => {};
  }

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(morgan('combined'));

  app.get('/', (req, res) => {
    RandomWord((error, data) => {
      if (error) return res.send(error);
      res.status(201).json(data);
    });
  });

  const server = app.listen(8080, () => {
    console.log('Server started on port 8080');
    callback(null, server, app);
  });
};
