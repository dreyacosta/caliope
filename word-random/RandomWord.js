'use strict';

const unirest = require('unirest');

function RandomWord() {
  return (callback) => {
    unirest.get('https://wordsapiv1.p.mashape.com/words/?random=true')
      .header('X-Mashape-Key', 'V6HJAHMXs7mshsexf3e6xUVkBeU9p11DvA0jsn5KjxWexOQf6K')
      .header('Accept', 'application/json')
      .end((result) => {
        console.log('Result:', result);
        callback(null, result.body);
      });
  };
}

module.exports = RandomWord;
